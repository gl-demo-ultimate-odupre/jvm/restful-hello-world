package fr.odupre.restfulhelloworld;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.when;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.boot.test.mock.mockito.MockBean;

@WebMvcTest(GreetingController.class)
public class GreetingControllerWebLayerTest {
	@Autowired
	private MockMvc mockMvc;
	@MockBean
	private GreetingService service;

	@Test
	public void shouldReturnDefaultMessage() throws Exception {
		when(service.greet("You")).thenReturn("Hello, You");

		this.mockMvc.perform(get("/greeting")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello, You")));
	}

	@Test
	public void shouldReturnCustomMessage() throws Exception {
		when(service.greet("world")).thenReturn("Hello, World");

		this.mockMvc.perform(get("/greeting?name=world")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("Hello, World")));
	}
}
