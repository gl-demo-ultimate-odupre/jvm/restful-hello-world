package fr.odupre.restfulhelloworld;

public record Greeting(long id, String content) { }
