package fr.odupre.restfulhelloworld;

import org.springframework.stereotype.Service;
import org.joda.time.LocalTime;

@Service
public class GreetingService {
    private static final String template = "🤖 Built with Gitlab.\n :tanuki-heart: With Love 💝.\nHello, %s!\n⏰ It is %s.";

    public String greet(String name) {
        LocalTime currentTime = new LocalTime();

        return String.format(template, name, currentTime);
    }
}