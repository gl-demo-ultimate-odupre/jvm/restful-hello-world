package fr.odupre.restfulhelloworld;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {
    private final GreetingService service;
	private final AtomicLong counter = new AtomicLong();

    public GreetingController(GreetingService service) {
		this.service = service;
	}

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "You") String name) {
		return new Greeting(counter.incrementAndGet(), service.greet(name));
	}
}